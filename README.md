# olip-post-install

This playbook runs after [`olip-deploy`](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy):

* On RPi based devices, it (poorly) handles the "master"/"slave" states
* On all devices, it installs the tolling required to pull jobs from TM

## Warning

This playbook tasks will eventually get merged into [`olip-deploy`](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy).

## Initialization

To be fair, we **never** run this playbook manually.

Should you do so (for development testing purposes for instance),
a convenience script is provided (but not tested):

```shell
curl -sfL https://gitlab.com/bibliosansfrontieres/olip/olip-post-install/-/raw/master/go.sh | bash -s
```
