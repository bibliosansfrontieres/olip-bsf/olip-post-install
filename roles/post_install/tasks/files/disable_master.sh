#!/bin/bash

say() {
  >&2 echo "[+] $*"
}

if [ "$(id -u)" != 0 ]; then
  say "Error: this script must be run as root."
  exit 13  # EACCES
fi

say "Remove the flag that tells that the device is a master"
# This will allow Ansible playbook to play "iam_a_master_device" specific tasks
rm -f /opt/iam_a_master_device

say "Remove  olip-pre-install & olip-deploy flags files..."
rm -f /opt/olip-pre-install /opt/olip-deploy

# This flag is used by deploy.py script. It use to be usefull for CMAL device but it is 
# kind of legacy nowadays. However we keep it for now...
# Look at the code here: https://gitlab.com/bibliosansfrontieres/tm/factory_manager/blob/master/scripts/kea/deploy.py#L247
say "Create reboot flag file..."
touch /opt/rebooted

# Generate a fresh machine-id at next reboot
rm -f /etc/machine-id

say "Done. On next boot, the card will install the VPN."
say "If you need more than one OLIP deployment, this is the right time to duplicate the card."
