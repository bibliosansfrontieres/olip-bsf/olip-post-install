#!/bin/bash

say() {
  >&2 echo "[+] $*"
}

if [ "$(id -u)" != 0 ]; then
  say "Error: this script must be run as root."
  exit 13  # EACCES
fi

say "Kill all deploy.py processes..."
pkill -f deploy.py

say "Recreate master flag file..."
touch /opt/iam_a_master_device

say "Remove playbooks flag files..."
rm -f /opt/olip-pre-install /opt/olip-deploy /opt/olip-post-install

say "Remove tinc configuration..."
rm -rf /etc/tinc/

/usr/local/bin/deploy.py &
say "Done. deploy.py started: "
tail -F /var/log/ansible-pull.log
